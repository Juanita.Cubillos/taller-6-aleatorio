//Replica programa

//REST
import express from 'express';
let app=new express();

//Rutas
function saludar(peticion, respuesta){
    respuesta.send("Hola API");
}

app.get('/saludo',saludar);

//Ruta
function calcular(req, res){
    let miSumador=new Sumador();
    let resultado=miSumador.sumar(56,78);

    //Devolver el resultado de una operación

    res.send(resultado.toString());
}

app.get('/calculadora',calcular);

//Encender el servidor de la API


class Sumador{
    sumar(a,b){
        return a+b;
    }
}



//Taller generar número aleatorio entre 1 y 100

function aleatorio(pet,resp){
    
    let miAleatorio=new Aleatorio();
    let resultado2=miAleatorio.getAleatorio(100,1);

    resp.send(resultado2.toString());

}
app.get('/aleatorio',aleatorio)

app.listen(3000);


class Aleatorio{
    
    getAleatorio(max, min){

        return Math.round(Math.random() * (max - min) + min);
        
    }

}